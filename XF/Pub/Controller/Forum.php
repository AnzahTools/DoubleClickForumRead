<?php

namespace AnzahTools\DoubleClickForumRead\XF\Pub\Controller;

use XF\Mvc\ParameterBag;

class Forum extends XFCP_Forum
{
	public function actionMarkReadNoCheck(ParameterBag $params)
	{
		$visitor = \XF::visitor();
		if (!$visitor->user_id)
		{
			return $this->noPermission();
		}

		$markDate = $this->filter('date', 'uint');
		if (!$markDate)
		{
			$markDate = \XF::$time;
		}

		$forumRepo = $this->getForumRepo();

		$lookup = $params->node_id ?: $params->node_name;
		if ($lookup)
		{
			$forum = $this->assertViewableForum($lookup);
		}
		else
		{
			$forum = null;
		}

		$forumRepo->markForumTreeReadByVisitor($forum, $markDate);

		return $this->redirect(
			$this->buildLink('forums', $forum),
			\XF::phrase('forum_x_marked_as_read', ['forum' => $forum->title])
		);
	}
}
